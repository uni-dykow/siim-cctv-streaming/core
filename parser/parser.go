package parser

import (
	"gitlab.com/uni-uni/siim-cctv-streaming/core/config"
	"strconv"
)

type ParsedConf struct {
	camerasNum  int
	command     []string
	inputLayout string
	uniqUrls    map[string]void
}

type void struct{}

func NewParsedConf(conf *config.Config) *ParsedConf {
	pc := ParsedConf{0, nil, "", map[string]void{}}
	pc.ParseConfig(conf)
	return &pc
}

func (pc *ParsedConf) CamerasNum() string {
	return strconv.Itoa(pc.camerasNum)
}

func (pc *ParsedConf) Command() []string {
	return pc.command
}

func (pc *ParsedConf) InputLayout() string {
	return pc.inputLayout
}

func (pc *ParsedConf) UniqUrlsAsStrings() []string {
	var keys []string
	for key := range pc.uniqUrls {
		keys = append(keys, key)
	}

	return keys
}

func (pc *ParsedConf) ParseConfig(conf *config.Config) {
	pc.command = []string{}
	var allUrls []string
	for _, elem := range conf.Config {
		if elem.Enabled == true {
			pc.appendUrlIfUniq(&elem)
			allUrls = append(allUrls, elem.Url)
		}
	}
	pc.camerasNum = len(allUrls)
	pc.buildCommand()
}

func (pc *ParsedConf) appendUrlIfUniq(elem *config.ConfigItem) {
	if !pc.existsInUrls(elem.Url) {
		pc.uniqUrls[elem.Url] = void{}
	}
}

func (pc *ParsedConf) existsInUrls(val string) bool {
	if _, ok := pc.uniqUrls[val]; ok {
		return true
	}
	return false
}

func (pc *ParsedConf) buildCommand() {
	pc.command = append(pc.command, pc.buildInputs()...)
	pc.command = append(pc.command, "-filter_complex")
	pc.command = append(pc.command, pc.buildXstack())
	pc.command = append(pc.command, "-map")
	pc.command = append(pc.command, "[s0]")
	pc.command = append(pc.command, "-c:a")
	pc.command = append(pc.command, "aac")
	pc.command = append(pc.command, "-c:v")
	pc.command = append(pc.command, "libx264")
	pc.command = append(pc.command, "-f")
	pc.command = append(pc.command, "flv")
	pc.command = append(pc.command, "-preset")
	pc.command = append(pc.command, "faster")
	pc.command = append(pc.command, "rtmp://localhost:1935/live-out/test")
	pc.command = append(pc.command, "-y")
}

func (pc *ParsedConf) buildInputs() []string {
	var inputs []string
	for _, url := range pc.UniqUrlsAsStrings() {
		inputs = append(inputs, "-analyzeduration")
		inputs = append(inputs, "20M")
		inputs = append(inputs, "-probesize")
		inputs = append(inputs, "10M")
		inputs = append(inputs, "-i")
		inputs = append(inputs, url)
	}

	return inputs
}

func (pc *ParsedConf) buildXstack() string {
	var xstack string
	for i := 0; i < pc.camerasNum; i++ {
		var index int
		if len(pc.uniqUrls) == 1 {
			index = 0
		} else {
			index = i
		}

		xstack += "[" + strconv.Itoa(index) + "]"
	}
	pc.mapCameraPositions()

	xstack += "xstack=inputs=" + strconv.Itoa(pc.camerasNum) + ":layout=" + pc.inputLayout + "[s0]"
	return xstack
}

// https://trac.ffmpeg.org/wiki/Create%20a%20mosaic%20out%20of%20several%20input%20videos%20using%20xstack
func (pc *ParsedConf) mapCameraPositions() {
	var layouts = map[int]string{
		0: "",
		1: "0_0",
		2: "0_0|0_h0",
		3: "0_0|0_h0|0_h0+h1",
		4: "0_0|0_h0|w0_0|w0_h0",
	}
	pc.inputLayout = layouts[pc.camerasNum]
}
