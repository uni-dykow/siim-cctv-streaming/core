package api

import (
	"encoding/json"
	"fmt"
	"gitlab.com/uni-uni/siim-cctv-streaming/core/storage"
	"log"
	"net/http"

	"gitlab.com/uni-uni/siim-cctv-streaming/core/config"
	"gitlab.com/uni-uni/siim-cctv-streaming/core/processing"
)

type api struct {
	conf *config.Config
	url  string
	proc *processing.Proc
}

func NewApi(config *config.Config, url string) *api {
	a := api{config, url, nil}
	return &a
}

func (a *api) Setup() {
	http.HandleFunc("/", a.configHandler)
	go func() {
		err := http.ListenAndServe(a.url, nil)
		if err != nil {
			log.Fatal("ListenAndServe: ", err)
		}
	}()
}

func (a *api) SetProc(proc *processing.Proc) {
	a.proc = proc
}

func (a *api) configHandler(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	switch r.Method {
	case "GET":
		a.getConfig(&w)
	case "POST":
		a.setConfig(&w, r)
	case "OPTIONS":
		w.WriteHeader(http.StatusOK)
	default:
		a.handleErr(&w)
	}
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func (a *api) getConfig(w *http.ResponseWriter) {
	j, _ := json.Marshal(a.conf)
	(*w).Write(j)
}

func (a *api) setConfig(w *http.ResponseWriter, r *http.Request) {
	d := json.NewDecoder(r.Body)
	conf := &config.Config{}
	err := d.Decode(conf)
	if err != nil {
		http.Error(*w, err.Error(), http.StatusInternalServerError)
	}
	a.conf = conf
	storage.StoreConfig(conf)
	go a.proc.ReloadStream(conf)
}

func (a *api) handleErr(w *http.ResponseWriter) {
	(*w).WriteHeader(http.StatusMethodNotAllowed)
	fmt.Fprintf(*(w), "I can't do that.")
}
