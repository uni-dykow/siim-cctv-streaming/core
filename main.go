package main

import (
	"gitlab.com/uni-uni/siim-cctv-streaming/core/api"
	"gitlab.com/uni-uni/siim-cctv-streaming/core/processing"
	"gitlab.com/uni-uni/siim-cctv-streaming/core/storage"
)

func main() {
	conf := storage.ReadConfig()

	a := api.NewApi(conf, "localhost:8090")
	a.Setup()

	p := processing.NewProc(conf)
	a.SetProc(p)
	p.StartStream()

	for true {
	}
}
