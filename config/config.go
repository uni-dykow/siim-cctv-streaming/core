package config

type ConfigItem struct {
	Id      int    `json:"id"`
	Url     string `json:"url"`
	Enabled bool   `json:"enabled"`
}

type Config struct {
	Config []ConfigItem `json:"config"`
}
